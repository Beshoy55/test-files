<?php
    include "./php/GetItems.php";
    $ItemsObj = new Items();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
        <title>Product List</title>
    </head>
    <body style="min-height:95vh; display:flex; flex-direction:column;">
    <form method="POST" action="./php/IndexPage.php" autocomplete="off">
        <header class="row align-items-center">
            <div class="offset-md-1 col-md-3">
                <h1>Product List</h1>
            </div>
            <div class="col-auto offset-md-5 col-md-1">
                <button class="btn btn-primary" type="submit" name="Add">ADD</button>
            </div>
            <div class="col-auto col-md-auto">
                <button class="btn btn-danger" type="submit" name="Delete" id="delete-product-btn">MASS DELETE</button>
            </div>
            <hr></hr>
        </header>
        <div class="container">
            <div class="row g-5" id="ObjectsContainer">
                <?php                    
                    $ItemsObj->DisplayItems();                    
                ?> 
            </div>
        </div>
    </form>   
    <footer class="row" style="margin-top:auto;">
        <hr></hr>
        <div class="col-12 text-center">
            <h6>Scandiweb Test assignment</h6>
        </div>
    </footer>     
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js" integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/" crossorigin="anonymous"></script>
    </body>
</html>