const SKUinput = document.getElementById("sku");
const Nameinput = document.getElementById("name");
const Priceinput = document.getElementById("price");

const DVDdiv = document.getElementById("DVD");
const Bookdiv = document.getElementById("Book");
const Furniturediv = document.getElementById("Furniture");

const DVDInput = document.getElementById("size");
const BookInput = document.getElementById("weight");
const FurnitureInput1 = document.getElementById("height");
const FurnitureInput2 = document.getElementById("width");
const FurnitureInput3 = document.getElementById("length");

const FormEle = document.getElementById("product_form");

var InputsToCheck = [SKUinput, Nameinput, Priceinput, DVDInput];

class ResetItem {
    constructor(){
        DVDdiv.style.display="none";
        Bookdiv.style.display="none";
        Furniturediv.style.display="none";
        DVDInput.removeAttribute("required");
        BookInput.removeAttribute("required");
        FurnitureInput1.removeAttribute("required");
        FurnitureInput2.removeAttribute("required");
        FurnitureInput3.removeAttribute("required");
        InputsToCheck = [];
        InputsToCheck.push(SKUinput, Nameinput, Priceinput);
    }    
}

class DVDItem extends ResetItem{
    MakeAction(){
        DVDdiv.style.display="block";
        DVDInput.setAttribute("required", "true");
        InputsToCheck.push(DVDInput);
    }
}

class BookItem extends ResetItem{
    MakeAction(){
        Bookdiv.style.display="block";
        BookInput.setAttribute("required", "true");
        InputsToCheck.push(BookInput);
    }
}

class FurnitureItem extends ResetItem{
    MakeAction(){
        Furniturediv.style.display="block";
        FurnitureInput1.setAttribute("required", "true");
        FurnitureInput2.setAttribute("required", "true");
        FurnitureInput3.setAttribute("required", "true");
        InputsToCheck.push(FurnitureInput1);
        InputsToCheck.push(FurnitureInput2);
        InputsToCheck.push(FurnitureInput3);
    }
}

const dict = new Map([['DVD', DVDItem], ['Book', BookItem], ['Furniture', FurnitureItem]]);

function TypeSelector(ItemClass){
    var UniqueItemInstance = new (dict.get(ItemClass))();
    UniqueItemInstance.MakeAction();
}

function CheckShowAlert(){
    var WillAlert = false;
    InputsToCheck.forEach(element => {
        if (element.value == ""){
            WillAlert = true;
        }
    });
    if (WillAlert){
        alert('Please, submit required data');
        return false;
    }        
}