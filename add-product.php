<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
            <title>Add Product</title>
        </head>
        <body style="min-height:95vh; display:flex; flex-direction:column;">
            <form method="POST" action="./php/AddProductPage.php" id="product_form" autocomplete="off">
                <header class="row align-items-center">
                    <div class="offset-md-1 col-md-3">
                        <h1>Product Add</h1>
                    </div>
                    <div class="col-auto offset-md-5 col-md-1">
                        <button class="btn btn-success" type="submit" name="submit" onclick="CheckShowAlert()">Save</button>
                    </div>
                    <div class="col-auto col-md-1">
                        <button class="btn btn-secondary" type="submit" name="cancel" formnovalidate>Cancel</button>
                    </div>                
                    <hr></hr>
                </header>                
                <div class="row gy-2">
                    <div class="align-items-center">
                        <label class="offset-md-1 col-md-1">SKU</label>
                        <input type="text" name="sku" id="sku" class="col-md-2" required>
                    </div>
                    <div class="align-items-center">
                        <label class="offset-md-1 col-md-1">Name</label>
                        <input type="text" name="name" id="name" class="col-md-2" required>
                    </div>
                    <div class="align-items-center">
                        <label class="offset-md-1 col-md-1">Price ($)</label>
                        <input type="number" name="price" id="price" class="col-md-2" step="any" required>
                    </div>
                    <div class="align-items-center">
                        <label class="offset-md-1 col-md-1">Type Switcher</label>
                        <select name="productType" id="productType" onchange="TypeSelector(value)">
                            <option value="DVD">DVD</option>
                            <option value="Book">Book</option>
                            <option value="Furniture">Furniture</option>
                        </select>
                    </div>
                    <div id="DVD">
                        <label class="offset-md-1 col-md-1">Size (MB)</label>
                        <input type="number" name="size" id="size" class="col-md-2" step="any" required>
                        <br><br>
                        <h6 class="offset-md-1">Please provide the DVD size in (MB)</h6>
                    </div>
                    <div id="Book" style="display:none">
                        <label class="offset-md-1 col-md-1">Weight (KG)</label>
                        <input type="number" name="weight" id="weight" class="col-md-2" step="any">
                        <br><br>
                        <h6 class="offset-md-1">Please provide the book weight in (KG)</h6>
                    </div>
                    <div class="row gy-2" id="Furniture" style="display:none">
                        <div>
                            <label class="offset-md-1 col-md-1">Height (CM)</label>
                            <input type="number" name="height" id="height" class="col-md-2" step="any">
                        </div>
                        <div>
                            <label class="offset-md-1 col-md-1">Width (CM)</label>
                            <input type="number" name="width" id="width" class="col-md-2" step="any">
                        </div>
                        <div>
                            <label class="offset-md-1 col-md-1">Length (CM)</label>
                            <input type="number" name="length" id="length" class="col-md-2" step="any">
                        </div>
                        <br>
                        <h6 class="offset-md-1">Please provide the furniture dimensions in (CM)</h6>
                    </div>
                </div>
            </form>
            <footer class="row" style="margin-top:auto;">
                <hr></hr>
                <div class="col-12 text-center">
                    <h6>Scandiweb Test assignment</h6>
                </div>
            </footer>     
            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js" integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/" crossorigin="anonymous"></script>
            <script src = js/addproducts.js></script>
        </body>
    </html>