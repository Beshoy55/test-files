<?php
    include "MySqlLogic.php";

    $sku;
    $name;
    $price;
    $type;

    abstract class product {        
        function __construct() {   
            global $sku, $name, $price, $type;
            $sku = $_POST['sku'];
            $name = $_POST['name'];
            $price = $_POST['price'];
            $type = strtoupper($_POST['productType']);
        }
    }


    class DVD extends product{
        public function TableSet() {
            global $type;
            $special = "Size: ".$_POST['size']." MB";
            $typeclassName = "Add" . $type;
            $AddClass = new $typeclassName;
            $AddClass->AddItem($special);
        }
    }

    class BOOK extends product{
        public function TableSet() {
            global $type;
            $special = "Weight: ".$_POST['weight']." KG";
            $typeclassName = "Add" . $type;
            $AddClass = new $typeclassName;
            $AddClass->AddItem($special);
        }
    }

    class FURNITURE extends product{
        public function TableSet() {
            global $type;
            $specialHeight = $_POST['height'];
            $specialWidth = $_POST['width'];
            $specialLength = $_POST['length'];
            $special = "Dimension: ".$specialHeight."x".$specialWidth."x".$specialLength;
            $typeclassName = "Add" . $type;
            $AddClass = new $typeclassName;
            $AddClass->AddItem($special);
        }
    }
?>