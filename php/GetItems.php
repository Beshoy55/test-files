<?php
    include "MySqlLogic.php";
    
    class Items {
        public function DisplayItems(){
            $SqlObject = new MysqlDB();
            $TableItems = $SqlObject->SendItems();
            while ($row = mysqli_fetch_array($TableItems)){
            ?>
                <div class="col-sm-4 col-md-3">
                    <div class="border border-2 border-secondary">
                        <input type="checkbox" name="del_check_box[]" value=<?php echo $row['id']?> class="delete-checkbox ms-2">
                        <div class="text-center px-2 pt-4 pb-5">
                            <span><?php echo $row['sku']?></span><br>
                            <span><?php echo $row['name']?></span><br>
                            <span><?php echo ($row['price']." $")?></span><br>
                            <span><?php echo $row['special']?></span>               
                        </div>
                    </div>         
                </div>                
            <?php
            }     
        }
    }
?>